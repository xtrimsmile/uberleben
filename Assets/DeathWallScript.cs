﻿using UnityEngine;
using System.Collections;

public class DeathWallScript : MonoBehaviour
{
  void OnTriggerEnter2D(Collider2D other)
  {
    if (((int)Mathf.Pow(2, other.gameObject.layer) & GameInstanse.Instanse.GameInfo.PlayerLayer.value) != 0)
    {
      GameInstanse.Instanse.GameInfo.SetPlayerDead();
    }
  }
}
