﻿using UnityEngine;
using System.Collections;

public class DestroyerScript : MonoBehaviour
{
  void OnTriggerExit(Collider other)
  {
    DestroyGameObject(other.gameObject);
  }

  private void DestroyGameObject(GameObject gameObject)
  {
    if (gameObject.transform.parent != null)
    {
      DestroyGameObject(gameObject.transform.parent.gameObject);
    }
    else
    {
      Destroy(gameObject, 1);
    }
  }
}
