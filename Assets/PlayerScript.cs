﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
  public bool Alive { get; set; }

  public Vector3 ZeroLevel { get; set; }

  private Rigidbody _rigidbody;

  // Use this for initialization
  void Start()
  {
    Alive = true;
    _rigidbody = gameObject.GetComponent<Rigidbody>();
  }

  // Update is called once per frame
  void Update()
  {
    if (Alive)
    {
#if UNITY_EDITOR
      
      var left = Input.GetKey(KeyCode.LeftArrow);
      var right = Input.GetKey(KeyCode.RightArrow);
      var up = Input.GetKey(KeyCode.UpArrow);
      var down = Input.GetKey(KeyCode.DownArrow);

      _rigidbody.velocity = new Vector3((left && !right  ? -3 : (!left && right ? 3 : 0)), (up && !down ? 3 : (!up && down ? -3 : 0)), _rigidbody.velocity.z);
#else

      _rigidbody.velocity = new Vector3((Input.acceleration.x - ZeroLevel.x) * 5, (Input.acceleration.y - ZeroLevel.y) * 5, _rigidbody.velocity.z);
#endif
    }
  }
}
