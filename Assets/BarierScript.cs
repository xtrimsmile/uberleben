﻿using UnityEngine;
using System.Collections;

public class BarierScript : MonoBehaviour
{
  public Collider2D BarierDeathCollider;

  void OnTriggerEnter(Collider other)
  {
    if (((int)Mathf.Pow(2, other.gameObject.layer) & GameInstanse.Instanse.GameInfo.PlayerLayer.value) != 0)
    {
      Debug.Log("Enter 3d barier collider");
      BarierDeathCollider.enabled = true;

      GameInstanse.Instanse.Spawner.SpawnBarier();
    }
  }

  void OnTriggerExit(Collider other)
  {
    if (((int)Mathf.Pow(2, other.gameObject.layer) & GameInstanse.Instanse.GameInfo.PlayerLayer.value) != 0)
    {
      Debug.Log("Exit 3d barier collider");
      BarierDeathCollider.enabled = false;

      GameInstanse.Instanse.GameInfo.AddPoints(1);
    }
  }
}
