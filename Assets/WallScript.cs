﻿using UnityEngine;
using System.Collections;

public class WallScript : MonoBehaviour {

	void OnTriggerEnter(Collider other)
  {
    if (((int)Mathf.Pow(2, other.gameObject.layer) & GameInstanse.Instanse.GameInfo.PlayerLayer.value) != 0)
    {
      GameInstanse.Instanse.Spawner.SpawnWall();
    }
  }
}
