﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GameInstanse
{
  private static GameInstanse _instanse;

  public static GameInstanse Instanse
  {
    get
    {
      if (_instanse == null)
      {
        _instanse = new GameInstanse();
      }
      return _instanse;
    }
  }

  private Game _gameInfo;
  public Game GameInfo
  {
    get
    {
      if (_gameInfo == null)
        _gameInfo = GameObject.Find("Game").GetComponent<Game>();
      return _gameInfo;
    }
  }

  private SpawnerScript _spawner;
  public SpawnerScript Spawner
  {
    get
    {
      if (_spawner == null)
        _spawner = GameObject.Find("Game").GetComponent<SpawnerScript>();
      return _spawner;
    }
  }

  private GameObject _currentPlayer;
  public GameObject CurrentPlayer
  {
    get
    {
      if (_currentPlayer == null && GameInfo != null)
      {
        _currentPlayer = UnityEngine.Object.Instantiate(GameInfo.PlayerPrefab);
        SetPlayerZeroLevel();
      }
      return _currentPlayer;
    }
  }

  public Vector3 ZeroLevel { get; set; }

  public void CreateNew()
  {
    Application.LoadLevel("uber");
  }

  public void SetCurrentZeroLevel()
  {
    ZeroLevel = new Vector3(Input.acceleration.x, Input.acceleration.y);
    SetPlayerZeroLevel();
  }

  private void SetPlayerZeroLevel()
  {
    var playerScript = _currentPlayer.GetComponent<PlayerScript>();
    playerScript.ZeroLevel = ZeroLevel;
  }

  private GameInstanse()
  {
    Screen.sleepTimeout = SleepTimeout.NeverSleep;
  }
}