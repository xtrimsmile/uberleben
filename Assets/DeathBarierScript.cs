﻿using UnityEngine;
using System.Collections;

public class DeathBarierScript : MonoBehaviour
{
  private Collider2D _collider;

  private bool _initialized = false;
  public bool Initialized
  {
    get { return _initialized; }
    set { _initialized = value; }
  }

  // Use this for initialization
  void Start()
  {
    _collider = GetComponent<Collider2D>();
    _collider.enabled = false;

    Initialized = true;
  }

  // Update is called once per frame
  void Update()
  {

  }

  void OnTriggerEnter2D(Collider2D other)
  {
    if (!Initialized)
    {
      return;
    }

    Debug.Log("Enter 2d barier collider");
    if (((int)Mathf.Pow(2, other.gameObject.layer) & GameInstanse.Instanse.GameInfo.PlayerLayer.value) != 0)
    {
      GameInstanse.Instanse.GameInfo.SetPlayerDead();
    }
  }
}
