﻿using UnityEngine;
using System.Collections;

public class CameraFollowScript : MonoBehaviour
{
  public float DistanceFromPlayer = 2;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    var playerPosition = GameInstanse.Instanse.CurrentPlayer.transform.position;
    transform.position = new Vector3(playerPosition.x, playerPosition.y, playerPosition.z - DistanceFromPlayer);
  }
}
