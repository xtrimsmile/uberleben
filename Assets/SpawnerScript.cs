﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerScript : MonoBehaviour
{
  public GameObject WallSpawner;

  public GameObject BarierSpawner;

  public float WallDeltaZ = 10;

  public float BarierDeltaZ = 10;

  public float BarierMinDeltaZ = 4;

  public float BarierDeltaShift = 0.2f;

  public int StartCountOfElements = 10;

  public GameObject WallPrefab;

  public GameObject[] BarierPrefabs;

  private List<GameObject> _wallsPool;

  private int _wallsCount = 0;

  private Dictionary<int, List<GameObject>> _bariersPool;

  private Dictionary<int, int> _bariersCount;

  private int _previousBarierNumber = -1;

  public void InitLevel()
  {
    _bariersPool = new Dictionary<int, List<GameObject>>();
    _bariersCount = new Dictionary<int, int>();
    for (int i = 0; i < BarierPrefabs.Length; i++)
    {
      _bariersPool[i] = new List<GameObject>();
      _bariersCount[i] = 0;
      // magic number 3
      for (int j = 0; j < 3; j++)
      {
        var barier = (GameObject)Instantiate(BarierPrefabs[i], new Vector3(), new Quaternion());
        barier.SetActive(false);
        _bariersPool[i].Add(barier);
        _bariersCount[i]++;
      }
    }

    _wallsPool = new List<GameObject>();
    _wallsCount = 0;
    for (int i = 0; i < StartCountOfElements + 3; i++)
    {
      var wall = (GameObject)Instantiate(WallPrefab, new Vector3(), new Quaternion());
      wall.SetActive(false);
      _wallsPool.Add(wall);
      _wallsCount++;
    }

    for (int i = 0; i < StartCountOfElements; i++)
    {
      SpawnWall();
      SpawnBarier();
    }
  }

  public void SpawnWall()
  {
    var spawnerPosition = WallSpawner.transform.position;

    if (_wallsCount == 0)
    {
      Instantiate(WallPrefab, spawnerPosition, new Quaternion());
    }
    else
    {
      var wall = _wallsPool[0];
      wall.transform.position = spawnerPosition;
      wall.SetActive(true);

      _wallsPool.RemoveAt(0);
      _wallsCount--;
    }

    WallSpawner.transform.position = new Vector3(spawnerPosition.x, spawnerPosition.y, spawnerPosition.z + WallDeltaZ);
  }

  public void DestroyWall(GameObject wall)
  {
    wall.SetActive(false);
    _wallsPool.Add(wall);
    _wallsCount++;
  }

  public void SpawnBarier()
  {
    var spawnerPosition = BarierSpawner.transform.position;
    var prefabNumber = (int)Random.Range(0, BarierPrefabs.Length);
    if (prefabNumber == _previousBarierNumber)
    {
      prefabNumber = (prefabNumber + 1) % BarierPrefabs.Length;
    }
    _previousBarierNumber = prefabNumber;

    if (_bariersCount[prefabNumber] == 0)
    {
      Instantiate(BarierPrefabs[prefabNumber], spawnerPosition, new Quaternion());
    }
    else
    {
      var barier = _bariersPool[prefabNumber][0];
      barier.transform.position = spawnerPosition;
      barier.SetActive(true);

      _bariersPool[prefabNumber].RemoveAt(0);
      _bariersCount[prefabNumber]--;
    }

    BarierSpawner.transform.position = new Vector3(spawnerPosition.x, spawnerPosition.y, spawnerPosition.z + BarierDeltaZ);
    if (BarierDeltaZ > BarierMinDeltaZ)
      BarierDeltaZ = Mathf.Max(BarierDeltaZ - BarierDeltaShift, BarierMinDeltaZ);
  }

  public void DestroyBarier(GameObject barier)
  {
    var info = barier.GetComponent<BarierInfoScript>();
    barier.SetActive(false);
    _bariersPool[info.Number].Add(barier);
    _bariersCount[info.Number]++;
  }
}
