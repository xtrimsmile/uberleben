﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
  public LayerMask PlayerLayer;

  public GameObject PlayerPrefab;

  public float FallForce = 50f;

  public Text Counter;

  private GameObject _playerInstance;

  private Rigidbody _playerBody;

  private PlayerScript _playerScript;

  private int _points = 0;

  // Use this for initialization
  void Start()
  {
    var spawner = GameInstanse.Instanse.Spawner;
    if (spawner != null)
    {
      spawner.InitLevel();
    }

    _playerInstance = GameInstanse.Instanse.CurrentPlayer;
    if (_playerInstance != null)
    {
      _playerBody = _playerInstance.GetComponent<Rigidbody>();
      _playerBody.AddForce(new Vector3(0, 0, FallForce));

      _playerScript = _playerInstance.GetComponent<PlayerScript>();
    }

  }

  // Update is called once per frame
  void Update()
  {
    if (!_playerScript.Alive && (Input.touchCount > 0 || Input.GetMouseButtonDown(0)))
    {
      Destroy(_playerInstance);
      GameInstanse.Instanse.CreateNew();
    }

    Counter.text = _points.ToString();
  }

  public void StopPlayer()
  {
    _playerBody.velocity = new Vector3();
  }

  public void SetPlayerDead()
  {
    var animator = _playerInstance.gameObject.GetComponentInChildren<Animator>();
    animator.SetBool("IsDead", true);
    _playerScript.Alive = false;

    StopPlayer();
  }

  public void ZeroAngleButtonClick()
  {
    GameInstanse.Instanse.SetCurrentZeroLevel();
  }

  public void AddPoints(int countOfPoints)
  {
    _points += countOfPoints;
  }
}
